//
//  ViewController.swift
//  CashReturn
//
//  Created by Bhumi on 12/8/17.
//  Copyright © 2017 shivlab. All rights reserved.
//

import UIKit

var dicNotes = NSMutableDictionary()

class ViewController: UIViewController {

    @IBOutlet weak var textfieldBillAmount: UITextField!
    
    @IBOutlet weak var textfieldPaidAmount: UITextField!
    
    @IBOutlet weak var labelReturnDesc: UILabel!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }
    
    // MARK: Button Action Method
    
    @IBAction func buttonSubmitClicked(_ sender: Any) {
        
        dicNotes.removeAllObjects()
        self.view.endEditing(true)
        
        if ((textfieldBillAmount.text == "") && (textfieldPaidAmount.text == "")) {
            
        }else{
            calculateDifferenceAmount()
        }
        
    }
    
    // MARK: Helper Methods
    
    func calculateDifferenceAmount(){
        
        // get enterd bill amount
        let billAmount: Float = (textfieldBillAmount.text! as NSString).floatValue
        
        // get enterd paid amount
        let paidAmount: Float = (textfieldPaidAmount.text! as NSString).floatValue
        
        var diffAmount: Float
        
        var strStatement: String = ""
        
        // Compare two amount to find difference here
        if paidAmount == billAmount
        {
            // case : user has paid exact amount of bill
            strStatement = "No need to give anything return"
        }
        else if paidAmount > billAmount
        {
             // case : user has paid extra amount from bill
            
            diffAmount = paidAmount - billAmount
            
            // find difference amount till diffamount is 0
            while diffAmount != 0
            {
                diffAmount = findDifferenceWithThisAmount(number: diffAmount)
            }
            
            let sortedValues = dicNotes.allKeys.sorted { (value1, value2) in
                let order = (value1 as AnyObject).compare(value2 as! String, options: .numeric)
                return order == .orderedAscending
            }
            
            for skey in sortedValues//dicNotes.allKeys
            {
                strStatement = "\(strStatement) \n \(dicNotes.value(forKey: skey as! String) ?? "") currency of \(skey)"
            }
            
            print(strStatement)
        }
        else
        {
            // case : user has paid less money
            strStatement = "Not enough money"
        }
        
        labelReturnDesc.text = strStatement
    }
    
    func findDifferenceWithThisAmount(number: Float) -> Float{
        
        // number is difference of amount of bill and paid amount
        
        // start extracting amount from highest currency
        
        var diffAmount: Float = 0
        
        if number >= 1000
        {
            diffAmount = number - 1000
            setDesiredValue(usingKey: "1000")
        }else if number >= 500{
            diffAmount = number - 500
            setDesiredValue(usingKey: "500")
        }else if number >= 200 {
            diffAmount = number - 200
            setDesiredValue(usingKey: "200")
        }else if number >= 100 {
            diffAmount = number - 100
            setDesiredValue(usingKey: "100")
        }else if number >= 50 {
            diffAmount = number - 50
            setDesiredValue(usingKey: "50")
        }else if number >= 20 {
            diffAmount = number - 20
            setDesiredValue(usingKey: "20")
        }else if number >= 10 {
            diffAmount = number - 10
            setDesiredValue(usingKey: "10")
        }else if number >= 5 {
            diffAmount = number - 5
            setDesiredValue(usingKey: "5")
        }else if number >= 2 {
            diffAmount = number - 2
            setDesiredValue(usingKey: "2")
        }else if number >= 1 {
            diffAmount = number - 1
            setDesiredValue(usingKey: "1")
        }else if number >= 0.5 {
            diffAmount = number - 0.5
            setDesiredValue(usingKey: "0.5")
        }
        
        return diffAmount
    }
    
    func setDesiredValue(usingKey keyValue:String)
    {
        // If already 1 available then add 1 more
        if let val = dicNotes.value(forKey: keyValue)
        {
            var valInt = (val as! NSString).intValue
            valInt += 1
            dicNotes.setValue("\(valInt)", forKey: keyValue)
        }
        else
        {
            // If not available then add 1
            let valInt: Int = 1
            dicNotes.setValue("\(valInt)", forKey: keyValue)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

